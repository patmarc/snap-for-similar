// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs

$(document).foundation();


var Snap = {

    trendsComplet: false,
    
    piecesComplet: false,

    trendImgPath: 'img/trends/',


    init: function(){

        //Hide for each section except the first one 
        //the titles and buttons filters elements

        $('.extra-elements').each(function(i, obj){
           
           if(i != 0){

                $(this).hide();
           }
            

        });

    },

    openSection: function(el){
        
        if(!$(el).hasClass('active') && $(el).hasClass('complet') ){
            
            $('.section').removeClass('active');

            $(el).addClass('active');

            Snap.switchActiveElements($(el), 'on');

            $(el).find('.previous').show();

            $('.thumbnail').show();

            //Thumbnail hidden when a section is active

            $(el).find('.thumbnail').hide();

        }
    
    },

    openPreviousSection: function(el){
        
        var previousSection = $(el).parent().parent().prev();

        $('.section').removeClass('active');
        
        previousSection.addClass('active');

        Snap.switchActiveElements(previousSection, 'on');

        //Thumbnail hidden when a section is active

        previousSection.find('.thumbnail').hide();

        //Top scrolling 

        var sectionID = previousSection.attr('id');
        
        var scrollVal = 190;
 
        if(sectionID === 'pieces'){
            
            scrollVal = 350;
        }
        
        $('body').animate({ scrollTop: scrollVal }, 600);

    },

    displayPieces: function(el){

        var trend = $(el).attr('id');

        $('#trend').removeClass('active');
        
        $('#pieces').addClass('active');

        $('#pieces .thumbnail').hide();

        //Remove thumbnail for previously selected item in pieces section

        if($('#pieces .thumbnail:empty').length == 0){
            
            $('#pieces .thumbnail').empty();
        }

        Snap.switchActiveElements('#pieces', 'on');
        
        $('.items-list').addClass('hide');

        //Show pieces acccording trend selected

        $('.' + trend).removeClass('hide');

        //Enable openSection method for trend and pieces sections

        Snap.trendsComplet = true;

        if(Snap.trendsComplet){
            
            $('#trend, #pieces').addClass('complet');
        }

        //Reset filter buttons

        $('.btn-filter').removeClass('selected');

        $('body').animate({ scrollTop: 210 }, 600);
        
    },

    displayMatches: function(el){

        //Change this value to display more items
        var matchesLimit = 8;

        var productID = $(el).attr('id');

        var url = "http://www.snapfashion.co.uk/api/similar/?format=jsonp&filter_brands=4&brand=4&external_id=" + productID + "";

        $('body').animate({ scrollTop: 350 }, 600);

        $('#pieces').removeClass('active');

        $('#matches').addClass('active');

        Snap.switchActiveElements('#matches', 'on');

        //Remove products list if already exists

        $('.product').remove();

        $('#status').show();

        //Reset filter buttons

        $('.btn-filter').removeClass('selected');

        $('.filters').hide();


        $.ajax({
            
            type: 'GET',
            
            url: url,
            
            jsonpCallback: 'jsonCallback',
            
            contentType: 'application/json',
            
            dataType: 'jsonp',
            
            success: function(json) {
                
                // Check if json.error is set and report the error if it is
                if (json.error != undefined) {
                    
                    console.error(json.error);
                    
                    return;
                }
                
                // Check that the items object contains 1 or more items in it
                try {
                    if ($.isEmptyObject(json.results.items)) {
                        
                        throw "No matching items found";
                    }
                }catch(err) {
                    
                    console.error(err);
                    
                    alert(err);
                        
                    return;
                }
                
                
                // Get the items object from the response so we can use it as an items hash array
                var items = json.results.items;
                var j = 0;

                $.each(json.results.combined, function(i, key_rank_pair) {
                    
                    if(i < matchesLimit){

                        // Grab the item key and rank and use the item key as an index on the items hash array to get the product information
                        $.each(key_rank_pair, function(item_key, rank) {
                
                            var snapResults = "<li class='product top-result animated fadeIn hide'><a href='" + items[item_key].purchase_url + "' target='_blank' ><img width='110' height='170' src='" + items[item_key].image_url + "' border='0' alt=''/></a><div class='product-info'><a href='" + items[item_key].purchase_url + "'><p class='product-title'>" + items[item_key].product_name + "</p>" + "</div></div>" + "</div></li>"
                    
                            $(snapResults).appendTo(".matches-list");
                           
                            Snap.filterState();
                        });
                    }
                    
                    // $('li.product.top-result').removeClass('hide');
                    
                });

                $.each(json.results.color_texture, function(i, key_rank_pair) {
                    
                    if(i < matchesLimit){

                        // Grab the item key and rank and use the item key as an index on the items hash array to get the product information
                        $.each(key_rank_pair, function(item_key, rank) {
                
                            var snapResults = "<li class='product colour animated fadeIn hide'><a href='" + items[item_key].purchase_url + "' target='_blank' ><img width='110' height='170' src='" + items[item_key].image_url + "' border='0' alt=''/></a><div class='product-info'><a href='" + items[item_key].purchase_url + "'><p class='product-title'>" + items[item_key].product_name + "</p>" + "</div></div>" + "</div></li>"
                    
                            $(snapResults).appendTo(".matches-list");

                            Snap.filterState();
                  
                        });
                    }

                    // $('li.product.colour').removeClass('hide');
                   
                });
              
                // Cycle through all key/rank pairing objects in the shape results array
                $.each(json.results.shape, function(i, key_rank_pair) {
                    
                    if(i < matchesLimit){
                    
                        // Grab the item key and rank and use the item key as an index on the items hash array to get the product information
                        $.each(key_rank_pair, function(item_key, rank) {
                        
                            var snapResults = "<li class='product shape animated fadeIn hide'><a href='" + items[item_key].purchase_url + "' target='_blank' ><img width='110' height='170' src='" + items[item_key].image_url + "' border='0' alt=''/></a><div class='product-info'><a href='" + items[item_key].purchase_url + "'><p class='product-title'>" + items[item_key].product_name + "</p>" + "</div></div>" + "</div></li>"
                                
                            $(snapResults).appendTo(".matches-list");

                            Snap.filterState();
                          
                        });
                    }
                   
                    // $('li.product.shape').removeClass('hide');

                });


                if($('ul.matches-list').find('li.top-result').length != 0){

                    $('li.product.top-result').removeClass('hide');

                }
                else if($('ul.matches-list').find('li.top-result').length == 0 && $('ul.matches-list').find('li.colour').length != 0){

                    $('li.product.colour').removeClass('hide');
                }
                else {
                    
                    $('li.product.shape').removeClass('hide');
                }


                $('#status').fadeOut();

                //Display buttons filters

                if($(window).width() < 800){

                    $('#filters-mobile').fadeIn();
                }
                else{

                    $('#filters-desktop').fadeIn();
                }
                
                
            },

            error: function(e) {
               
               console.log(e.message);
            }
        });

        //Enable openSection method for matches section

        Snap.piecesComplet = true;

        if(Snap.piecesComplet){
            
            $('#matches').addClass('complet');
        }

    },

    switchActiveElements: function(section, state){
   
        $('.extra-elements').each(function(i, obj){
           
           $(this).hide();

        });

        if(state === 'on'){

            $(section).find('.extra-elements').show();
        }
        else if(state == 'off'){

            $(section).find('.extra-elements').hide();
        } 
    },

    //Method to switch between Top results, colours and shapes.

    filterResult: function(el){

        var button = $(el).attr('id');

        //Reset filter buttons

        $('.btn-filter').removeClass('selected');

        $('.product').addClass('hide');

        if(button === 'top-result'){

            $('.'+ button).addClass('selected');

            $('li.product.top-result').removeClass('hide');

        }
        else if(button === 'colour'){

            $('.'+ button).addClass('selected');

            $('li.product.colour').removeClass('hide');

        }
        else if(button === 'shape'){

            $('.'+ button).addClass('selected');

            $('li.product.shape').removeClass('hide');

        }

    },

    filterState: function(){

        //Reset filter buttons

        $('.top-result').show();
        $('.colour').show();
        $('.shape').show();
        
        var parent = $('ul.matches-list');

        //if has class top results 
        if(parent.find('li.top-result').length != 0){

            $('.top-result').addClass('selected');

            $('li.product.top-result').removeClass('hide');

        }
        else if(parent.find('li.top-result').length == 0 && parent.find('li.colour').length != 0){

            $('.colour').addClass('selected');

            $('.top-result').hide();
        }
        else {
            
            $('.shape').addClass('selected');

            $('.top-result').hide();
            $('.colour').hide();
        }
        
    },

    displayThumbnail: function(el, section){

        //Restore thumbnail divs if previously hidden when section got activated

        $('.thumbnail').show();

        var thumbnail = $(el).attr('id');

        var thumbnailHTML = null;

        if(section === 'trends'){

            thumbnailHTML = '<img src="' + Snap.trendImgPath + thumbnail + '.jpg">';
            
            if($('#trend .thumbnail:empty').length == 0){

                $('#trend .thumbnail').empty();    
            }

            $('#trend .thumbnail').append(thumbnailHTML);
        }
        else if(section === 'matches'){

            if($('#pieces .thumbnail:empty').length == 0){

                $('#pieces .thumbnail').empty();    
            }

            thumbnailHTML = '<img src="' + 

            'http://media.oasis-stores.com/pws/client/images/catalogue/products/' + thumbnail + '/list4/' + thumbnail + '.jpg">';

            $('#pieces .thumbnail').append(thumbnailHTML);
        }


    }

}

$(document).ready(function(){

    Snap.init();


    $('.previous').click(function(event){
        
        event.stopPropagation();

        Snap.openPreviousSection(this);
       
    });

    $('.section').click(function(){
        
        Snap.openSection(this);

    });

    $('.trend li').click(function(event){

        event.stopPropagation(event);

        Snap.displayThumbnail(this, 'trends');

        Snap.displayPieces(this);
    });

    $('.items-list li').click(function(event){

        event.stopPropagation(event);

        Snap.displayThumbnail(this, 'matches');

        Snap.displayMatches(this);


    });

    $('.btn-filter').click(function(event){

        event.stopPropagation(event);

        Snap.filterResult(this);


    });


});

